import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Debit} from './pages/Debit';
import { Navigation } from './components/Nav';
import 'bootstrap/dist/css/bootstrap.min.css';
import { AddDepense } from './pages/FormulaireAdd';

import Home from './pages/Home';

function App() {
  return (
    <Router>
      <div className="container-fluid">
        <Navigation />
        <Switch>
        <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/Debit">
            <Debit />
          </Route>
          <Route path="/AddDepense">
            <AddDepense />
          </Route>
        </Switch>

      </div>
    </Router>
  );
}

export default App;

