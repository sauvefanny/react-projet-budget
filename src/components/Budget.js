import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { ModalFetchById } from './ModalFetchById';



export function Budget({ budget, onDelete }) {

  const [show, setShow] = useState(false);
  const handleModal = (param) => setShow(param);

  return (

    <Card style={{ width: '18rem', margin:'auto'}}>
      <Card.Img variant="top" src="" />

      <Card.Body>

        <Card.Title>{budget.categorie}</Card.Title>

        <Card.Text>{budget.titre}</Card.Text>

        <Card.Text>{budget.date_depense}</Card.Text>

        <Card.Text>{budget.montant_depense}</Card.Text>

        <Button variant="primary" onClick={() => handleModal(true)}>
          Details</Button>

        <button className="btn btn-danger" onClick={() => onDelete(budget.id)}>X</button>

      </Card.Body>

      <section className="row">
        <ModalFetchById show={show} onClose={handleModal} paramBudget={budget} />
      </section>

    </Card>
  )
}
