import { useState } from 'react';
import '../components/Form.css'


const initial = {
    categorie: '',
    titre: '',
    date: '',
    montant:''
};

export function FormDepense({ onSubmit}) {

    const [form, setForm] = useState(initial);

    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event){
        event.preventDefault();
        onSubmit(form);
    }

    return (
        <form onSubmit={handleSubmit} className= "form" >

            <h1>Ajouter une dépense</h1>

            <label> Catégorie :</label>
            <input className="form-control" required type="text" name="categorie" onChange={handleChange}  />

            <label> Titre : </label>
            <input className="form-control" type="text" name="titre" onChange={handleChange}  />

            <label> Date de la dépense : </label>
            <input className="form-control" required type="date" name="date_depense" onChange={handleChange}/>

            <label> Montant : </label>
            <input className="form-control" type="number" pattern="[0-9]" name="montant_depense" onChange={handleChange}/>

            <button className='btn'>Ajouter</button>

        </form>

    )
}