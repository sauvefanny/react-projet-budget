import '../components/Nav.css'
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';


export function Navigation() {
    return (

        <Nav id = 'nav' className="justify-content-center  mb-3">
            <Nav.Item>
                <Link to='/'> Accueil</Link>
            </Nav.Item>
            
            <Nav.Item>
               <Link to='/Debit'> Dépenses</Link>
            </Nav.Item>

            <Nav.Item>
            <Link to='/AddDepense'>   Ajouter une Dépense</Link>
            </Nav.Item>

        </Nav>

    )
}