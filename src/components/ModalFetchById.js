import Modal from 'react-bootstrap/Modal';
import React from "react";
import Button from 'react-bootstrap/Button';


export function ModalFetchById({ show, paramBudget , onClose}) {

    return (
      
      <>
        <Modal show={show} >

          <Modal.Header>
            <Modal.Title>{paramBudget.categorie}</Modal.Title>
          </Modal.Header>

          <Modal.Body>{paramBudget.date_depense}</Modal.Body>

          <Modal.Footer>
            <Button variant="primary" onClick={()=>{onClose(false)}}>
              Close
            </Button>
          </Modal.Footer>

        </Modal>
      </>
    );
  }
