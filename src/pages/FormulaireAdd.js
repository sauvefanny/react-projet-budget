import { FormDepense } from "../components/FormDepense";
import { BudgetService } from "../shared/depenses-service";
import { useState } from "react";


export function AddDepense() {

    const [depenses, setDepenses] = useState([]);

    async function handleAdd(depense){
       const response = await BudgetService.add(depense);
        setDepenses([
            ...depenses,
            response.data
        ]);
    }

    return (
        <div>
            <FormDepense onSubmit={handleAdd} />
        </div>
    )
}