import React from 'react';
import { useEffect, useState } from 'react';
import { BudgetService } from '../shared/depenses-service';
import { Budget } from '../components/Budget';

export function Debit() {

  const [budgets, setBudgets] = useState([]);

  async function fetchBudgets() {
    const response = await BudgetService.fetchAll();

    setBudgets(response.data);

  }

  async function deleteBudget(id) {
    await BudgetService.delete(id)

    setBudgets(
      budgets.filter(item => item.id !== id)
    );
  }
  
  useEffect(() => {
    fetchBudgets();

  }, []);


  return (
    <div>
      <h1>Récapitulatif des dépenses</h1>

      <section className="row">

        {budgets.map(item =>

          <Budget
            key={item.id}
            budget={item}
            onDelete={deleteBudget}
          />

        )}
      </section>



    </div>
  )
}
export default Debit