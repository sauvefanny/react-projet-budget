import { BudgetService } from '../shared/depenses-service';
import { useState } from 'react';
import { useEffect } from 'react';
import React from 'react';
import './Home.css';




function Home() {

  const [count, setCount] = useState(0);

  async function FetchTotal() {

    const response = await BudgetService.total();

    setCount(response.data)
  }

  useEffect(() => {

    FetchTotal();

  }, []);

  return (


    <div className="container">
      <div className="row">
        <div className='col-12'>
          <h1>Total de vos Opérations</h1>

          <p className='total'>{count}</p>
        </div>
      </div>
    </div>
  )

}
export default Home

