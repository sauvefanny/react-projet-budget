import axios from "axios";

export class BudgetService {

    static async fetchAll() {
        return axios.get(process.env.REACT_APP_SERVER_URL + '/api/budget/all');
    }

    static async add(depense) {
        return axios.post(process.env.REACT_APP_SERVER_URL + '/api/budget', depense)
    }

    static async delete(id) {
        axios.delete(process.env.REACT_APP_SERVER_URL + '/api/budget/' + id);
    }

    static async fetchById(id) {
        return axios.get(process.env.REACT_APP_SERVER_URL + '/api/budget/' + id)
    }

    static async total(){
        return axios.get(process.env.REACT_APP_SERVER_URL + '/api/budget/total')
    }

}